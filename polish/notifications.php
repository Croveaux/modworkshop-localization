<?php
$l['myalerts_pm'] = '{1} Wysłał Ci Wiadomośc Prywatną Nazwaną <b>"{2}"</b>.';
$l['myalerts_quoted'] = '{1} Zacytował Cię W <b>"{2}"</b>.';
$l['myalerts_post_threadauthor'] = '{1} Odpowiedział Na twój Wątek <b>"{2}"</b>. Po tym może być więcej postów.';
$l['myalerts_subscribed_thread'] = '{1} Odpowiedział Na Zasubskrybowany wątek <b>"{2}"</b>.';
$l['myalerts_rated_threadauthor'] = '{1} Ocenił Twój wątek <b>"{2}"</b>.';
$l['myalerts_voted_threadauthor'] = '{1} Zagłosował w Ankiecie z <b>"{2}"</b>.';
$l['alert_mod_updated'] = "{1} Zaktualizowano Tryb Obserwowania {2}";
$l['alert_subbed_discussion_modpage'] = "{1} Doda Komentarz Na modyfikacji {2} Którą Zasubskrybowałeś";
$l['no_alerts_found'] = "Nie Znaleziono Żadnego Powiadomienia";
$l['no_more_alerts_found'] = "Nie można było znaleść więcej Powiadomień";
$l['notifications'] = 'Powiadomienia';
$l['browse_all_notifications'] = 'Przeglądaj wszystkie Powiadomienia';
