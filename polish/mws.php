<?php

$l['mydownloads_cancel'] = 'Anuluj';
$l['mydownloads_cant_edit_comment'] = 'Musisz zaczekać {1} sekund przed edytowaniem';
$l['mydownloads_categories'] = "Kategorie";
$l['mydownloads_category'] = 'Kategoria';
$l['mydownloads_collaborating'] = 'Współpraca';
$l['mydownloads_date'] = 'Data';
$l['mydownloads_delete'] = 'Usuń';
$l['mydownloads_downloads'] = 'Pobrane';
$l['mydownloads_downloads_number'] = "{1} mod(y)";
$l['mydownloads_download_collaborator'] = 'kolaborator';
$l['mydownloads_download_delete_comment'] = "Usuń";
$l['mydownloads_download_edit_comment'] = "Edytuj";
$l['mydownloads_download_name'] = "Nazwa";
$l['mydownloads_download_views'] = "Wyświetlenia";
$l['mydownloads_edit'] = 'Edytuj Modyfikacje';
$l['mydownloads_editing_download'] = 'Edytuj';
$l['mydownloads_empty_reason'] = 'Musisz Podać Powód Dla Zreportowania Modyfikacji.';
$l['mydownloads_enter_a_comment'] = 'Prosze Dać Opis';
$l['mydownloads_error'] = 'Error';
$l['mydownloads_error_attachsize'] = "Plik Który Dołączyłeś Jest Zbyt Duży. Maksymalna Wielkość to {1} kilobajtów.";
$l['mydownloads_error_attachtype'] = "Typ Pliku Który Dołączyłeś Jest Niedozwolony. Prosze Użyj Inny Typ Pliku.";
$l['mydownloads_error_invalidrating'] = "Dałeś Nieważną Ocene Dla Tej Modyfikacji.";
$l['mydownloads_error_uploadfailed'] = "Przesyłanie Pliku się Nie powiodło. Prosze Wybiierz Inny plik i spróbuj ponownie. ";
$l['mydownloads_error_uploadfailed_detail'] = "błąd detali: ";
$l['mydownloads_error_uploadfailed_lost'] = "Załącznik nie Mógł Być Znaleziony Na Serwerze.";
$l['mydownloads_error_uploadfailed_movefailed'] = "Wystąpił Poroblem Z Przenoszeniem Pliku Do Miejsca Docelowego.";
$l['mydownloads_error_uploadfailed_nothingtomove'] = "Niepoprawny Plik był Wybrany, Więc Plik nie Mógł ByćPrzeniesiony Do Miejsca Docelowego.";
$l['mydownloads_error_uploadfailed_php1'] = "PHP returned: Plik Przekroczył upload_max_filesize Dyrektowany w php.ini.  Prosze Zgłoś Się z tym Błędem Do Administratora.";
$l['mydownloads_error_uploadfailed_php2'] = "Plik Przekroczył Maksymalny Limit Wielkości.";
$l['mydownloads_error_uploadfailed_php3'] = "Plik Był Częściowo Przesłany.";
$l['mydownloads_error_uploadfailed_php4'] = "Plik Nie był Przesłany.";
$l['mydownloads_error_uploadfailed_php6'] = "PHP returned: Brakujący Chwilowy Folder.  Prosze Zgłoś Się z tym Błędem Do Administratora.";
$l['mydownloads_error_uploadfailed_php7'] = "PHP returned: Nie Udało się Zapisać Pliku Na Dysku.  Prosze Zgłoś Się z tym Błędem Do Administratora.";
$l['mydownloads_error_uploadfailed_phpx'] = "PHP returned błąd z kodem: {1}.  Prosze Zgłoś Się z tym Błędem Do Administratora.";
$l['mydownloads_error_uploadsize'] = "Plik Jest Za duży.";
$l['mydownloads_exceeded'] = 'PHP Przekroczono limit Wielkości. Maksymalny to {1}.';
$l['mydownloads_author'] = 'Autor';
$l['mydownloads_flood_check'] = "Chcesz Napisać Kolejny Komentarz Zaszybko. Prosze Poczekaj {1} sekund."; // copied from MyBB :P
$l['mydownloads_hidden'] = 'ukryty';
$l['mydownloads_history'] = 'Historia Pobierania';
$l['mydownloads_invalid_banner'] = 'Ustawiłeś Nieprawidłowy baner. Dozwolone Rozszerzenia to: gif, png, jpeg, apng, and webp.';
$l['mydownloads_non_secure_banner'] = 'Ustawiłeś Baner Który Nie jest Wspomogany przez HTTPS. Nasza Strona Używa Tylko HTTPS. Uzyj Banera Który Używa HTTPS.';
$l['mydownloads_invalid_extension'] = 'Zdjęcie Może mieć Tylko Rozszerzenia: jpeg, png, webp, apng, or gif.';
$l['mydownloads_invalid_url'] = 'Link jest Niedostępny/Nieważny: ';
$l['mydownloads_invite_only'] = "Tylko Przez Zaproszenie";
$l['mydownloads_last_updated'] = 'Ostatnio Aktualizowane';
$l['mydownloads_log_in_register'] = "Żeby Zostawić komentarz musisz Być zalogowanym.";
$l['mydownloads_max_height'] = 'Twoje Zdjęcie jest Wyższe Niż {1}px.';
$l['mydownloads_max_files'] = 'Możesz mieć tylko {1} Plików Na Każdą Modyfikacje.';
$l['mydownloads_max_previews'] = 'Możesz Dodać tylko {1} Zdjęć Dla Tej Modyfikacji.';
$l['mydownloads_max_files_error'] = 'Nie Możesz Dodać Więcej Plików Dla Tej Modyfikacji Ponieważ Osiągnąłeś limit.';
$l['mydownloads_max_previews_error'] = 'Nie Możesz Dodać Więcej Zdjęć Dla Tej Modyfikacji Ponieważ Osiągnąłeś limit.';
$l['mydownloads_max_res'] = 'Twoje Zdjęcia Są ograniczone do {1}px Długości i {2}px Wysokości.';
$l['mydownloads_max_width'] = 'Długość Tegi Zdjęcia Jest Większa Niż {1}px.';
$l['mydownloads_awaiting_approval'] ='Oczekiwane Powierdzenie';
$l['mydownloads_new_comment'] = 'Nowy Komentarz Został Dodany Przez {1} Do Twojej Modyfikacji o nazwie {2}';
$l['mydownloads_not_invited'] = "Ta Modyfikacja \"{1}\" Jest Ustawiony na Tylko Zaproszonia.<br/>Musisz Być Zaproszony Żeby Zobaczyć Tą Modyfikacje. Żeby Dostać zaproszenie Skontaktuj się Z \"{2}\".";
$l['mydownloads_no_cid'] = "Kategoria Którą Wybrałeś Nie Jest Dostępna.";
$l['mydownloads_no_description'] = "Nie Dodałeś Opisu!.";
$l['mydownloads_no_did'] = "Modyfikacja Którą Wybrałeś Jest Nie Dostępna.";
$l['mydownloads_no_fid'] = "Plik Który Wybrałeś Nie Jest Dostępny.";
$l['mydownloads_no_dl_name'] = "Nie Dodałeś Nazwy Dla Pliku.";
$l['mydownloads_no_mod_name'] = "Nie Dodałeś Nazwy Dla Modyfikacji.";
$l['mydownloads_no_dl_type'] = "Nie Wybrałeś Kategori Pliku.";
$l['mydownloads_desc_too_long'] = 'Opis Nie Może Być Dłuższy Niż 220 Znaków.';
$l['mydownloads_no_dl_visibility'] = 'Wybrałeś Nie Dostępną Widoczność Modyfikacji.';
$l['mydownloads_no_permissions'] = "Nie Masz Pozwolenia Żeby To Zrobić.";
$l['mydownloads_no_files'] = 'Brak Plików';
$l['mydownloads_number_downloads'] = "Pobrania";
$l['mydownloads_options'] = 'Opcje';
$l['mydownloads_preview_empty'] = 'Pole Obrazka Jest Puste.';
$l['mydownloads_rate_banned'] = "Nie Możesz Oceniać Kiedy Jesteś Zbanowany.";
$l['mydownloads_rating_too_fast'] = 'Możesz Oceniać Tylko Co {1} sekund. Spróbuj ponownie w {2} sekund.';
$l['mydownloads_select_category'] = 'Wybierz kategorie';
$l['mydownloads_sortby'] = 'Sortuj Według';
$l['mydownloads_submit'] = 'Zatwierdź';
$l['mydownloads_upload_mod'] = 'Dodaj Modyfikacje';
$l['mydownloads_download_changelog'] = 'Dziennik Zmian';
$l['mydownloads_download_description'] = 'Opis';
$l['mydownloads_no_description'] = 'Brak Opisu.';
$l['mydownloads_no_name'] = 'Brak Nazwy';
$l['mydownloads_version'] = 'Wersja';
$l['mydownloads_sub_categories'] = 'Subkategorie';
$l['mydownloads_suspended'] = "Zawieszony";
$l['mydownloads_suspended_error'] = "Ta Modyfikacja Jest Zawieszona I Nie może Być Dostępna.";
$l['mydownloads_tags'] = 'Tagi';
$l['mydownloads_unlisted'] = "Nie katalogowany";
$l['mydownloads_upload_problem_pr_already_exists'] = "Obrazek Z Tą Samą Nazwą Został Już Wysłany";
$l['mydownloads_main'] = 'Główny';
$l['mydownloads_patch'] = 'Poprawka';
$l['mydownloads_addon'] = 'Addon';
$l['mydownloads_edited'] = "(Edytowane)";
$l['mydownloads_comment_mention'] = "Byłeś Wymieniony W Poście {1} W Modyfikacji {2}";
$l['mydownloads_guests_cannot_submit'] = 'Żeby Móć Dodawać Modyfikacje Musisz Być Zalogowanym. <a href="/login">Log in through Steam</a>';
$l['mydownloads_something_wrong'] = "Coś Poszło nie tak";
$l['mydownloads_bad_category'] = "Nie Masz Pozwolenia Żeby Dodać Coś W Tej Katergorii";
$l['mods_game'] = "Gra";
$l['mods_games'] = "Gry";
$l['mods_for'] = "{1} Modyfikacje";
$l['mods'] = "Modyfikacje";
$l['profile_pm'] = "Prywatna Wiadomość";
$l['report'] = "Report";
$l['report_desc'] = '
Jeśli Myślisz że {1} łamie <a href="/rules">rules</a>, Możesz To Zgłosić Dla Naszego Admina/Moderatora I To Sprawdzimy Najszybciej Jak Się da.
<br>
Masowe Flagowanie Nie Będzie tolerowane!
';
$l['no_perms'] = "Nie masz Pozwolenia Żeby To Zrobić";
$l['submit'] = "Zatwierdź";
$l['images'] = 'Obrazy';
$l['image'] = 'Obraz';
$l['files'] = 'Pliki';
$l['none'] = 'Żaden';
$l['tab_main'] = 'Główny';
$l['tab_extra'] = 'Ekstra';
$l['type'] = 'Typ';
$l['warning'] = 'UWAGA!';
$l['wiki'] = 'Wiki';
$l['discord'] = 'Discord';
$l['forums'] = 'Forum';
$l['support_us'] = 'Wspieraj Nas';
$l['support_mws'] = 'Wspieraj ModWorkshop';
$l['more'] = 'więcej';
$l['steam_group'] = 'Grupa Steam';
$l['steam_profile'] = 'Profil Steam';
$l['rules'] = 'Zasady';
$l['conduct'] = 'Kodeks Postępowania';
$l['privacy'] = 'Polityka Prywatności';
$l['terms'] = 'Warunki Usługi';
$l['about'] = 'O Nas';
$l['about_mws'] = 'O ModWorkshop';
$l['about_desc'] = "
ModWorkshop to platforma, która hostuje modyfikacje dla gier zapewniając niezbędne narzędzia do dzielenia się i tworzenia modów, narzędzi i pomysłów.

W 2013 zaczęliśmy jako LastBullet, miejscem numer jeden dla moddingu PAYDAY
Z czasem, przekształciliśmy się w ModWorkshop. Chcieliśmy się rozwinąć nie tylko z hostowania modów do PAYDAY. Teraz, hostujemy mody dla wielokrotnie różnorodnych gier takie jak PAYDAY 2, PAYDAY: The Heist, Noita, Enter The Gungeon i więcej!

Myślimy, że modding powinien być otwartym oprogramowaniem a nie jej zamkniętą powłoką. Nie ma sensu w moddowaniu, jeżeli mody nie są dostępne dla pozostałych majsterkowiczów.

To i owo, mamy regułę aby modyfikacje wymagały pozostania otwarte źródłowo do pewnego stopnia.

My absolutnie kochamy modding, i jeżeli chciałbyś ugościć modyfikację do byle jakiej gry, możesz to zrobić!
Jeżeli sekcja '[Other Games](/game/other)' nie jest wystarczająca, skontaktuj się z nami i możemy przedyskutować stworzenie sekcji dedykowanej dla tej gry!";
$l['my_profile'] = 'Profil';
$l['user_settings'] = "Ustawienia Użytkownika";
$l['logout'] = "Wyloguj Się";
$l['last_updates_games'] = "Ostatnio Zaktualizowane Gry";
$l['game_category'] = "Gra/Kategoria";
$l['likes'] = "Like'ów";
$l['no_mods_found'] = "Nie Znaleziono Modyfikacji";
$l['no_more_mods_found'] = "Nie Można było Znaleść Więcej Modyfikacji";
$l['no_users_found'] = "Nie Znaleziono Użytkowników";
$l['no_more_users_found'] = "Nie Można było Znaleść Więcej Użytkowników";
$l['load_more'] = "Załaduj więcej..";
$l['management'] = "Zarządzanie";
$l['moderators'] = "Moderatorzy";
$l['translators'] = "Translatorzy Strony";
$l['search_results'] = "Wyniki Wyszukiwania";
$l['search'] = "Szukaj";
$l['section_admins'] = "Administratorzy Sekcji";
$l['user'] = "Użytkownik";
$l['mod'] = "Mod";
$l['view_profile'] = "Zobacz Profil";
$l['view_steam_profile'] = "Zobacz Profil Steam";
$l['home'] = 'Strona główna';
$l['all'] = 'Wszystko';
$l['publish_date'] = 'Data opublikowania';
$l['ie_warn'] = "Wykryliśmy że używaż Inetnet Explorer, Nasza Strona Go Nie Wspiera Więc Mogą Występować Błędy. Prosze Użyć Nowszej Wyszukiwarki! ";
$l['return_to_top'] = "Wróć Na Góre";
$l['powered_by_mybb'] = 'Obsługiwane Przez <a href="https://mybb.com" target="_blank">MyBB</a>.';
$l['powered_by_steam'] = 'Login Powered By <a href="https://steampowered.com" target="_blank">Steam</a>.';
$l['search_mods_global'] = 'Wyszukuj modyfikacji Globalnie';
$l['search_mods_cat'] = 'Szukaj Modyfikacji W Katergorii';
$l['search_mods_game'] = 'Szukaj Modyfikacji W Grze';
$l['search_user'] = 'Wyszukuj Użytkownika';
$l['search_users'] = 'Szukaj Użytkownika(ów)';
$l['comments_disabled'] = 'Komentarze Zostały Wyłączone. Tylko "Opiekunowie" Modyfikacji i moderacja Mogą Dodawać Komentarze.';
$l['files_waiting'] = 'Plik(i) Czekają Na Zaakceptowanie';
$l['file_waiting'] = 'Czekanie Na Zaakceptowanie';
$l['file_approved'] = 'Zaakceptowane';
$l['popular_now'] = 'Popularne Teraz';
$l['banner'] = 'Baner';
$l['saving'] = 'Zapisywanie..';
$l['save'] = 'Zapisz';
$l['saved'] = 'Zapisano!';
$l['search_for_user'] = 'Szukaj Użytkownika..';
$l['site_desc'] = 'ModWorkshop To platforma Która Udostępnia Modyfikacje I Potrzebne Do Nich Narzędzia.';
$l['liked_mods'] = 'Ulubione Modyfikacje';
$l['pin'] = 'Przypnij';
$l['unpin'] = 'Odepnij';
$l['pinned'] = 'Przypięte';
$l['pin_confirm'] = 'Jesteś Pewien że Chcesz Przypiąć Ten Komentarz?';
$l['unpin_confirm'] = 'Jesteś Pewien Że Chcesz Odpiąć Ten Komentarz?';
$l['follow'] = 'Obserwuj';
$l['follow_cat_help'] = 'Obserwuj Tą Kategorie Żeby Zobaczyć Modyfikacje W Folderze Obserwowane Modyfikacje';
$l['follow_game_help'] = 'Obserwuj Tą Grę Żeby Widzieć Modyfikacje W Folderze Obserwowane Modyfikacje';
$l['follow_user_help'] = 'Obserwuj Tego Użytkownika żeby Zobaczyć Jego Modyfikacje W Twojej Liście Obserwowanych Modyfikacji';
$l['unfollow'] = 'Przestań Obserwować';
$l['subscribe'] = 'Zaubskrybuj';
$l['unsubscribe'] = 'Zrezygnuj Z Subskrybcji';
$l['followed'] = 'Obserwowane Modyfikacje';
$l['browse_all_messages'] = 'Przeglądaj Wszystkie Wiadomości';
$l['are_you_sure_undone'] = 'Czy Jesteś Pewien Że Chcesz To Zrobić ? Ta Akcja Nie Może Być Odwołana!';
$l['no_bio'] = "Brak Opisu.";
$l['default'] = "Domyśle";
$l['follow_all_subcats'] = "Czy Chcesz Obserwować Wszystkie SubKategorie Tej Gry?";
$l['upload_image'] = "Prześlij zdjęcie";
$l['error_users_only'] = "Ta Strona/Akcja Jest Tylko Dla Osób Zalogowanych. Zaloguj Się żeby Kontynuować.";
$l['plans'] = "Plany"; 
$l['until_time'] = "Aż do {1}"; 
$l['signin_through_steam'] = "Zaloguj się Przez Steam"; 
$l['last_pm_notice'] = 'Masz Jedną Nieprzeczytaną Wiadomość  <a href="/user/{1}">{2}</a> Z Nazwą <a href="/message/{3}">{4}</a>'; 
$l['private_profile_notice'] = 'Ten Profil Jest Prywatny'; 
$l['year'] = 'rok'; 
$l['years'] = 'lat'; 
$l['month'] = 'Miesiąc'; 
$l['months'] = 'Miesięcy'; 
$l['day'] = 'dzień'; 
$l['days'] = 'Dni';
$l['week'] = 'Tydzień'; 
$l['weeks'] = 'Tygodnie';
$l['hour'] = 'Godzina'; 
$l['hours'] = 'Godzin';
$l['minute'] = 'minuta'; 
$l['minutes'] = 'minut';
$l['second'] = 'sekunda'; 
$l['seconds'] = 'sekund';
$l['timeago'] = '{1} {2} Temu'; //{1} = Number {2} = Years/months/days/hour/minutes/seconds
$l['nothing_selected'] = 'Nic Nie Wybrane';
$l['no_results'] = "Brak Wyników Pasujących {0}";
$l['size_yb'] = "YB";
$l['size_zb'] = "ZB";
$l['size_eb'] = "EB";
$l['size_pb'] = "PB";
$l['size_tb'] = "TB";
$l['size_gb'] = "GB";
$l['size_mb'] = "MB";
$l['size_kb'] = "KB";
$l['size_b'] = "B";
$l['drop_files'] = "Upuść Tutaj Pliki żeby Je Przesłać";
$l['file_too_big'] = "File Plik Jest Za duży ({{filesize}}MiB). Max file Rozmiar: {{maxFilesize}}MiB.";
$l['invalid_file_type'] = "Nie Możesz Przesyłać Takich Plików.";
$l['server_response_error'] = "Serwer Odpowiedział Z {{statusCode}} code.";
$l['cancel_upload'] = "Anuluj Przesyłanie";
$l['cancel_upload_confirm'] = "Jesteś Pewien że Chcesz Anulować To Przesyłanie?";
$l['upload_canceled'] = "Przesyłanie Anulowane";
$l['max_files_exceeded'] = "Nie Możesz Przesyłać Więcej Plików.";
$l['currently_selected'] = "Obecnie Wybrany";
$l['select_and_type'] = "Wybierz i Zacznij Pisać";
$l['select_error'] = "Nie Można Dostać Wyników";
$l['select_query'] = "Zacznij Pisać Słowo Do Wyszukania";
$l['searching'] = "Szukanie";
$l['select_more_char'] = "Prosze Wpisz Więcej Liter";
$l['blue'] = "Niebieski";
$l['green'] = "Zielony";
$l['pink'] = "Różowy";
$l['red'] = "Czerwony";
$l['teal'] = "Morski";
$l['purple'] = "Fioletowy";
$l['gray'] = "Szary";
$l['orange'] = "Pomarańczowy";
$l['cyan'] = "cyjanowy";
$l['error_cant_reply'] = "Nie można Odpowiedzieć Na Wiadomość.";
$l['error_no_message'] = "Wiadomośc Nie istnieje.";
$l['error_no_user'] = "Użytkownik nie istnieje";
$l['error_too_quick'] = "Żądanie Dane Za szybko";
$l['error_invalid_user'] = "Nieznany Użytkownik";
$l['error_banned_message'] = "Zbanowani Nie Mogą Pisać Widaomości";
$l['error_banned_report'] = "Zbanowani Nie mogą reportować Modyfikacji!";
$l['error_commnet_too_long'] = "Komentarz Za Długi!";
$l['error_delete_comment'] = "Komentarz Nie istnieje Albo Został Usunięty";
$l['error_banned_submit'] = "Zbanowani Użytkownicy Nie Mogą Dodawać Modyfikacji!";
$l['error_banned_files'] = "Zbanowani Użytkownicy Nie mogą Przesyłać Plików!";
$l['error_image'] = "To Nie istnieje!";
$l['error_transfer_self'] = "Nie Możesz Przesłać Swojego Moda Dla Siebie!";
$l['error_no_collabs'] = "Ta Modyfikacja Nie Ma Współtwórców!";
$l['error_file_size'] = "Plik Jest Za Duży!";
$l['error_invalid_did'] = "Niepoprawny";
$l['error_mod_save'] = "Nie Udało Się Zapisać Modyfikacji: {0}";
$l['error_mod_create'] = "Nie Udało Się Stworzyć Modyfikacji: {0}";
$l['error_mod_no_files'] = "Modyfikacja Nie Ma Plików I Nie Może Być Publicznie Dostępna.";
$l['error_mod_waiting'] = "Modyfikacja CZeka Na Twoje Potwierdzenie.";
$l['error_moderator_page'] = "Ta Strona Jest TYLKO Dla Moderatorów!";
$l['error_404'] = "Strona nie istnieje";
$l['registration_date'] = "Registration Date";
$l['lastvisit'] = "Last Visit";
