<?php
$l['myalerts_pm'] = '{1}님이 당신에게 <b>"{2}"</b>라는 이름의 새로운 개인 메시지를 보냈습니다.';
$l['myalerts_quoted'] = '{1}님이 <b>"{2}"</b>에서 당신을 인용했습니다.';
$l['myalerts_post_threadauthor'] = '{1}님이 당신의 스레드 <b>"{2}"</b>에 답글을 달았습니다. 이것 말고도 더 많은 포스트가 있을겁니다.';
$l['myalerts_subscribed_thread'] = '{1}님이 당신이 구독한 스레드 <b>"{2}"</b> 에 답글을 달았습니다.';
$l['myalerts_rated_threadauthor'] = '{1}님이 당신의 스레드 <b>"{2}"</b>에 평점을 매겼습니다.';
$l['myalerts_voted_threadauthor'] = '{1}님이 당신의 여론조사 <b>"{2}"</b>에서 투표를 했습니다.';
$l['alert_mod_updated'] = "{1}님이 팔로우 중인 {2} 모드를 업데이트 했습니다";
$l['alert_subbed_discussion_modpage'] = "{1}님이 당신이 팔로우 하고 있던 {2} 모드에 댓글을 달았습니다.";
$l['no_alerts_found'] = "찾은 알림 없음";
$l['no_more_alerts_found'] = "알림을 더 찾을수 없습니다";
$l['notifications'] = '알림';
$l['browse_all_notifications'] = '모든 알림 탐색';
