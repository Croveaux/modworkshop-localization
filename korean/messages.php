<?php
$l['restore'] = "복구";
$l['move_to_trash'] = "휴지통으로 이동";
$l['compose'] = "메시지 쓰기";
$l['trash'] = "휴지통";
$l['inbox'] = "받은 메시지함";
$l['sent_messages'] = "보낸 메시지들";
$l['receiver'] = "수신자";
$l['sender'] = "전송자";
$l['actions'] = "행위";
$l['subject'] = "제목";
$l['message'] = "메시지";
$l['to'] = "에게";
$l['BCC'] = "BCC";
$l['no_messages_found'] = "찾은 메시지 없음";
$l['no_more_messages_found'] = "메시지를 더 찾지 못했습니다.";
$l['messages'] = "메시지들";
$l['send_message_banned'] = "밴 당한 상태에서는 매시지를 보낼수 없습니다.";
