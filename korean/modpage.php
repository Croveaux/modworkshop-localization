<?php

$l['mydownloads_being_updated'] = '업데이트 중';
$l['mydownloads_meta_by'] = '{1} by {2}'; //Mod X by Y;
$l['mydownloads_cannot_rate_own'] = '당신 모드에 평가를 매길수 없습니다.';
$l['mydownloads_collaborators_desc'] = '참여자들은 당신이 모드 수정 권환을 준 사람들입니다. 그들은 당신의 모드를 삭제 할수 없으며, 모드 소유권 양도, 밎 참여자 수정이 불가능합니다';
$l['mydownloads_comment_banned'] = "밴 당한 상태에서는 댓글을 달수 없습니다.";
$l['mydownloads_delete_confirm'] = "정말로 이 댓글을 삭제하겠습니까?";
$l['mydownloads_download_description'] = "설명";
$l['mydownloads_download_is_suspended'] = '
이 모드는 정지됐으며 오직 소유자(제작자)와 사이트 관리자들만 보입니다.
<br/>이 정지는 확인을 위한 일시적인 정지이거나 <a style="text-decoration:underline;" href="/rules">규칙</a>을 위반해서 영구적인 정지일수도 있습ㄴ디ㅏ.
<br/>정지에 대해서 관리 간부들에게 연락하거나 만약 당신의 모드가 규칙을 지키도록 업데이트 된 경우 "정지 해제 요청"을 보낵수 있습니다 <a style="text-decoration:underline;"  href="/forumdisplay.php?fid=66">here</a>.';
$l['mydownloads_download_changelog'] = '변경점';
$l['mydownloads_download'] = "다운로드";
$l['mydownloads_license'] = '라이센스';
$l['mydownloads_report_download'] = '모드 신고';
$l['show_download_link_warn'] = '수상한 링크를 클릭할때 조심하세요. 만약에 이 링크가 악성 링크라고 판단 되는경우, 관리자에게 신고하시오';
$l['show_download_link'] = '다운로드 링크 표시';
$l['show_files'] = '파일 표시';
$l['submitted_by'] = '제출자';
$l['subscribe_commnet_help'] = '이 댓글 섹션에 누군가 댓글을 달 경우 알림을 받으세요';
$l['follow_mod_help'] = '이 모드를 팔로우해서 팔로우 중인 모드에 표시하고 모드가 업데이트 될 경우 알림을 받으세요';
$l['mydownloads_download_comments'] = "댓글";
$l['mydownloads_unsuspend_it'] = '정지 해제';
$l['mydownloads_suspend_it'] = '정지';
$l['mydownloads_files_alert'] = '<strong>파일 없음</strong> | 모드에 파일이 없으므로,이 모드는 투명 상태입니다.';
$l['mydownloads_files_alert_waiting'] = '<strong>승인된 파일 없음</strong> | 이 모드는 현제 투명 상태이며, 당신이 업로드한 파일이 승인될때까지 투명 상태로 남습니다.';
$l['share'] = '공유';
